<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(PhilippineRegionTableSeeder::class);
        $this->call(PhilippineProvinceTableSeeder::class);
		$this->call(PhilippineCityTableSeeder::class);
		// $this->call(PhilippineAddressTableSeeder::class);
    }
}
